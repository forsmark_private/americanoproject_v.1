package com.example.americanoproject;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//Class: Shows UI for the tournament settings such as the amount of players that will participate and what score the games will play to.
public class TournamentSettings extends AppCompatActivity {

    //Variables to control number of players. If user chooses X amount of players we need to add X amount of players.
    private int numberOfPlayers = 0;
    private int numberOfPlayersAdded = 0;

    //Variable that controls the amount of score every match will go to.
    private int chosenScore = 0;

    //Temp list of players that will be copied and sent to Class:GameActivity.
    List<String> playerListTemp = new ArrayList<String>(){};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tournament_settings);

        //region Creates and sets the UI/Layout to variables
        Button fourPlayersBtn = (Button)findViewById(R.id.btn_tournamentSettings_4players);
        Button eightPlayersBtn = (Button)findViewById(R.id.btn_tournamentSettings_8players);
        Button twelvePlayersBtn = (Button)findViewById(R.id.btn_tournamentSettings_12players);
        Button sixteenPlayersBtn = (Button)findViewById(R.id.btn_tournamentSettings_16players);
        Button twentyPlayersBtn = (Button)findViewById(R.id.btn_tournamentSettings_20players);
        Button twentyfourPlayersBtn = (Button)findViewById(R.id.btn_tournamentSettings_24players);

        EditText addPlayerEditText = (EditText)findViewById(R.id.et_tournamentSettings_setPlayerName);
        Button addPlayerBtn = (Button)findViewById(R.id.btn_tournamentSetting_addPlayer);

        RadioButton sixteenPointRb = (RadioButton)findViewById(R.id.rb_TournamentSettings_16points);
        RadioButton twentyfourPointRb = (RadioButton)findViewById(R.id.rb_TournamentSettings_24points);
        RadioButton thirtytwoPointRb = (RadioButton)findViewById(R.id.rb_TournamentSettings_32points);

        Button startGameBtn = (Button)findViewById(R.id.btn_tournamentSettings_startGame);
        //endregion

        //region Listeners(Buttons) for choosing number of players. Changes the int:numberOfPlayers and enables the next steps. Highlights pressed button.
        fourPlayersBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setNumberOfPlayers(4);
                fourPlayersBtn.setAlpha(1f);
                eightPlayersBtn.setAlpha(0.5f);
                twelvePlayersBtn.setAlpha(0.5f);
                sixteenPlayersBtn.setAlpha(0.5f);
                twentyPlayersBtn.setAlpha(0.5f);
                twentyfourPlayersBtn.setAlpha(0.5f);
                addPlayerEditText.setEnabled(true);
                addPlayerBtn.setEnabled(true);
            }
        });

        eightPlayersBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setNumberOfPlayers(8);
                fourPlayersBtn.setAlpha(0.5f);
                eightPlayersBtn.setAlpha(1f);
                twelvePlayersBtn.setAlpha(0.5f);
                sixteenPlayersBtn.setAlpha(0.5f);
                twentyPlayersBtn.setAlpha(0.5f);
                twentyfourPlayersBtn.setAlpha(0.5f);
            }
        });

        twelvePlayersBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setNumberOfPlayers(12);
                fourPlayersBtn.setAlpha(0.5f);
                eightPlayersBtn.setAlpha(0.5f);
                twelvePlayersBtn.setAlpha(1f);
                sixteenPlayersBtn.setAlpha(0.5f);
                twentyPlayersBtn.setAlpha(0.5f);
                twentyfourPlayersBtn.setAlpha(0.5f);
            }
        });

        sixteenPlayersBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setNumberOfPlayers(16);
                fourPlayersBtn.setAlpha(0.5f);
                eightPlayersBtn.setAlpha(0.5f);
                twelvePlayersBtn.setAlpha(0.5f);
                sixteenPlayersBtn.setAlpha(1f);
                twentyPlayersBtn.setAlpha(0.5f);
                twentyfourPlayersBtn.setAlpha(0.5f);
            }
        });

        twentyPlayersBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setNumberOfPlayers(20);
                fourPlayersBtn.setAlpha(0.5f);
                eightPlayersBtn.setAlpha(0.5f);
                twelvePlayersBtn.setAlpha(0.5f);
                sixteenPlayersBtn.setAlpha(0.5f);
                twentyPlayersBtn.setAlpha(1f);
                twentyfourPlayersBtn.setAlpha(0.5f);
            }
        });

        twentyfourPlayersBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setNumberOfPlayers(24);
                fourPlayersBtn.setAlpha(0.5f);
                eightPlayersBtn.setAlpha(0.5f);
                twelvePlayersBtn.setAlpha(0.5f);
                sixteenPlayersBtn.setAlpha(0.5f);
                twentyPlayersBtn.setAlpha(0.5f);
                twentyfourPlayersBtn.setAlpha(1f);
            }
        });
        //endregion

        //region Listener(Button). Need same amount of presses as numberOfPlayers to enable next step. Each press adds new Player to list. First press disables the possibility to change number of players.
        addPlayerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fourPlayersBtn.setEnabled(false);
                eightPlayersBtn.setEnabled(false);
                twelvePlayersBtn.setEnabled(false);
                sixteenPlayersBtn.setEnabled(false);
                twentyPlayersBtn.setEnabled(false);
                twentyfourPlayersBtn.setEnabled(false);

                playerListTemp.add(addPlayerEditText.getText().toString());
                setNumberOfPlayersAdded();
                addPlayerEditText.setText(null);

                if(getNumberOfPlayers() == getNumberOfPlayersAdded()){
                    addPlayerEditText.setEnabled(false);
                    addPlayerBtn.setEnabled(false);
                    sixteenPointRb.setEnabled(true);
                    twentyfourPointRb.setEnabled(true);
                    thirtytwoPointRb.setEnabled(true);
                }
            }
        });
        //endregion

        //region Listeners(RB) to chose matchscore. Sends to activity_game. Enables startGameBtn button.
        sixteenPointRb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startGameBtn.setEnabled(true);
                setChosenScore(16);
            }
        });
        twentyfourPointRb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startGameBtn.setEnabled(true);
                setChosenScore(24);
            }
        });
        thirtytwoPointRb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startGameBtn.setEnabled(true);
                setChosenScore(32);
            }
        });
        //endregion

        //region Listener(Button) for starting the game. Last step in settings. Sends playerListTemp/numberOfPlayers and chosenScore to Class:GameActivity.
        startGameBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(TournamentSettings.this, GameActivity.class);
                intent.putExtra("playerListExtra", (Serializable) playerListTemp);
                //intent.putExtra("numberOfPlayersExtra", numberOfPlayers);
                intent.putExtra("chosenScoreExtra", chosenScore);
                startActivity(intent);
            }
        });
        //endregion

    }

    //region Getters & Setters methods
    //Getter & Setter for numberOfPlayers
    public int getNumberOfPlayers(){
        return numberOfPlayers;
    }
    public void setNumberOfPlayers(int numberOfPlayersIn){
        numberOfPlayers = numberOfPlayersIn;
    }

    //Getter and setter(+1) for numberOfPlayersAdded
    public int getNumberOfPlayersAdded(){
        return numberOfPlayersAdded;
    }
    public void setNumberOfPlayersAdded(){
        numberOfPlayersAdded += 1;
    }

    public int getChosenScore(){
        return chosenScore;
    }
    public void setChosenScore(int chosenScoreIn){
        chosenScore = chosenScoreIn;
    }
    //endregion

}