package com.example.americanoproject;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Creates and sets the UI/Layout to variables
        Button americanoBtn = (Button)findViewById(R.id.btn_main_regularAmericano);

        //Listener(Button) to start a new activity(Americano Tournament)
        americanoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, TournamentSettings.class));
            }
        });

    }
}