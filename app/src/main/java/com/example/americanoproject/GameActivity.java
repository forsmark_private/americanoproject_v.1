package com.example.americanoproject;

import android.content.Intent;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class GameActivity extends AppCompatActivity {

    private static final String TAG = "Tag";
    //Creates variables for UI/Layout
    Button nextTurnBtn;
    TextView playerChart;

    //List of playersName sent from TournamentSettings that will be used to create a list<Players>
    List<String> playerListTemp = new ArrayList<String>(){};
    //List of players to keep track of the score for each player
    List<Player> playerList = new ArrayList<Player>(){};
    //Integer containing the winner matchScore
    Integer matchScore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        //Receives the extra information from previous activity and stores it into variables and lists.
        matchScore = getIntent().getIntExtra("chosenScoreExtra", 0);
        playerListTemp =  (List<String>) getIntent().getSerializableExtra("playerListExtra");

        //Sets the UI/Layout to variables
        nextTurnBtn = (Button)findViewById(R.id.btn_game_nextTurn);
        playerChart = (TextView)findViewById(R.id.tv_game_playerChart);

        //Foreach loop that creates a new player for every string in playerListTemp and stores them in playerList.
        for(String s : playerListTemp){
            playerList.add(new Player(s));
        }

        nextTurnBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playerChart.setText(updateChart());
            }
        });

    }

    //Method to update playerChart. Dont want to change the original list because the playscheme is dependent of it. First create a new templist then return sorted playerinformation in a string.
    public String updateChart(){
        String returnString = "";
        List<Player> tempList = new ArrayList<Player>();
        for (Player p : playerList){
            tempList.add(p);
        }

        Collections.sort(tempList);

        for (Player pl : tempList){
            returnString += pl.showPlayerInformation();
        }

        return returnString;
    }

}